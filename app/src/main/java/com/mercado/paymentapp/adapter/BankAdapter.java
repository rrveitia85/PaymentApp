package com.mercado.paymentapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mercado.paymentapp.R;
import com.mercado.paymentapp.model.PaymenthMethodModel;

import java.util.ArrayList;

public class BankAdapter extends RecyclerView.Adapter<BankAdapter.ViewHolder>{

    private ArrayList<PaymenthMethodModel> transactionArrayList = new ArrayList<>();
    private Context context;
    private OnClickSelectedListener listener;

    public BankAdapter(){

    }

    public BankAdapter(Context context,ArrayList<PaymenthMethodModel> transactionArrayList,OnClickSelectedListener listener){
        this.context = context;
        this.transactionArrayList = transactionArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.items_payment_method_layout, viewGroup, false);
        return new BankAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        PaymenthMethodModel currentModelForm = getTransactionArrayList().get(i);
        viewHolder.namePaymentTv.setText(currentModelForm.getName());
        Glide.with(getContext()).load(currentModelForm.getSecure_thumbnail()).into(viewHolder.cardTypeIv);

        viewHolder.mediosPayBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getListener().onItemSelected(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(getTransactionArrayList() !=null){
            return getTransactionArrayList().size();
        }
        return 0;
    }

    public ArrayList<PaymenthMethodModel> getTransactionArrayList() {
        return transactionArrayList;
    }

    public void setTransactionArrayList(ArrayList<PaymenthMethodModel> transactionArrayList) {
        this.transactionArrayList = transactionArrayList;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public OnClickSelectedListener getListener() {
        return listener;
    }

    public void setListener(OnClickSelectedListener listener) {
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView cardTypeIv;
        TextView namePaymentTv;
        View mediosPayBox;
        public ViewHolder(View itemView){
            super(itemView);
            cardTypeIv = (ImageView)itemView.findViewById(R.id.cardTypeIv);
            namePaymentTv = (TextView)itemView.findViewById(R.id.namePaymentTv);
            mediosPayBox = itemView.findViewById(R.id.mediosPayBox);
        }
    }
}
