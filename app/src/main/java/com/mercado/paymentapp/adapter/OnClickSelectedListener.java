package com.mercado.paymentapp.adapter;

public interface OnClickSelectedListener {
    public void onItemSelected(int position);
}
