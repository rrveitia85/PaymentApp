package com.mercado.paymentapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mercado.paymentapp.R;
import com.mercado.paymentapp.model.FormOfPageModel;

import java.util.ArrayList;

public class PaymentMethodsAdapter extends RecyclerView.Adapter<PaymentMethodsAdapter.ViewHolder>{

    private ArrayList<FormOfPageModel> transactionArrayList = new ArrayList<>();
    private Context context;
    private OnClickSelectedListener listener;

    public PaymentMethodsAdapter(){

    }

    public PaymentMethodsAdapter(ArrayList<FormOfPageModel> transactionArrayListParam,Context contextParam,OnClickSelectedListener listener){
        this.transactionArrayList = transactionArrayListParam;
        this.context = contextParam;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.items_payment_method_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        FormOfPageModel currentModelForm = transactionArrayList.get(i);
        viewHolder.namePaymentTv.setText(currentModelForm.getName());
        Glide.with(context).load(currentModelForm.getSecure_thumbnail()).into(viewHolder.cardTypeIv);

        viewHolder.mediosPayBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemSelected(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(transactionArrayList!=null){
            return transactionArrayList.size();
        }
        return 0;
    }

    public ArrayList<FormOfPageModel> getTransactionArrayList() {
        return transactionArrayList;
    }

    public void setTransactionArrayList(ArrayList<FormOfPageModel> transactionArrayList) {
        this.transactionArrayList = transactionArrayList;
    }

    public void setData(ArrayList<FormOfPageModel> listTransactions){
        transactionArrayList = listTransactions;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView cardTypeIv;
        TextView namePaymentTv;
        View mediosPayBox;
        public ViewHolder(View itemView){
            super(itemView);
           cardTypeIv = (ImageView)itemView.findViewById(R.id.cardTypeIv);
           namePaymentTv = (TextView)itemView.findViewById(R.id.namePaymentTv);
           mediosPayBox = itemView.findViewById(R.id.mediosPayBox);
        }
    }
}
