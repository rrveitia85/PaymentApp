package com.mercado.paymentapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mercado.paymentapp.R;
import com.mercado.paymentapp.model.PayerCostModel;

import java.util.ArrayList;

public class RecommendedAdapter extends RecyclerView.Adapter<RecommendedAdapter.ViewHolder> {
    private ArrayList<PayerCostModel> recomendedList = new ArrayList<>();
    private Context context;
    private OnClickSelectedListener listener;

    public RecommendedAdapter(){}

    public RecommendedAdapter(Context context,ArrayList<PayerCostModel> recomendedList,OnClickSelectedListener listener){
        this.context = context;
        this.setRecomendedList(recomendedList);
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.items_recomended_message_layout, viewGroup, false);
        return new RecommendedAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder,final int i) {
        PayerCostModel currentModelForm = getRecomendedList().get(i);
        if(currentModelForm.getRecommended_message()!=null){
            viewHolder.nameRecomendedTv.setText(currentModelForm.getRecommended_message());
        }
        viewHolder.recomendeMesasageBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemSelected(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(getRecomendedList() !=null){
            return getRecomendedList().size();
        }
        return 0;
    }

    public ArrayList<PayerCostModel> getRecomendedList() {
        return recomendedList;
    }

    public void setRecomendedList(ArrayList<PayerCostModel> recomendedList) {
        this.recomendedList = recomendedList;
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView nameRecomendedTv;
        View recomendeMesasageBox;
        public ViewHolder(View itemView){
            super(itemView);
            nameRecomendedTv = (TextView)itemView.findViewById(R.id.nameRecomendedTv);
            recomendeMesasageBox = itemView.findViewById(R.id.recomendeMesasageBox);
        }
    }
}
