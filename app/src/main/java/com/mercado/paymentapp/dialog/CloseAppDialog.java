package com.mercado.paymentapp.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.mercado.paymentapp.R;

public class CloseAppDialog extends DialogFragment {

    public ListenerClickInterface listener;

    public void setListeners(ListenerClickInterface listenerParam){
      this.listener = listenerParam;
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity()).setTitle(R.string.close_app_title)
                .setMessage(getActivity().getResources().getString(R.string.close_app_message))
                .setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                      listener.onClickPositive(CloseAppDialog.this);
                    }
                })
                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        listener.onClickCancelar(CloseAppDialog.this);
                    }
                })
                .create();
    }
}
