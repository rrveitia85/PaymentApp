package com.mercado.paymentapp.dialog;

import android.support.v4.app.DialogFragment;

public interface ListenerClickInterface {
    public void onClickPositive(DialogFragment dialog);
    public void onClickCancelar(DialogFragment dialog);
}
