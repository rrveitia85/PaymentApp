package com.mercado.paymentapp.model;

import android.support.annotation.Nullable;

public class BinModel{
    private String pattern;
    private String installments_pattern;
    @Nullable
    private String exclusion_pattern;

    public BinModel(){
    }
    public BinModel(String ppattern,String pinstallments_pattern,@Nullable String pexclusion_pattern){
        this.pattern = ppattern;
        this.installments_pattern = pinstallments_pattern;
        this.exclusion_pattern = pexclusion_pattern;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getInstallments_pattern() {
        return installments_pattern;
    }

    public void setInstallments_pattern(String installments_pattern) {
        this.installments_pattern = installments_pattern;
    }

    @Nullable
    public String getExclusion_pattern() {
        return exclusion_pattern;
    }

    public void setExclusion_pattern(@Nullable String exclusion_pattern) {
        this.exclusion_pattern = exclusion_pattern;
    }
}
