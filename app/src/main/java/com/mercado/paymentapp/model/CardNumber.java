package com.mercado.paymentapp.model;

public class CardNumber {
   private String validation;
   private int length;

   public CardNumber(){}

   public CardNumber(String pcard_number,String pvalidation,int plength){
       this.validation = pvalidation;
       this.length = plength;
   }

    public String getValidation() {
        return validation;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
