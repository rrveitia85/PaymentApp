package com.mercado.paymentapp.model;

import java.util.ArrayList;

public class FormOfPageDataModel{
    private ArrayList<FormOfPageModel> list;

    public FormOfPageDataModel(){
        list =new ArrayList<>();
    }


    public ArrayList<FormOfPageModel> getList() {
        return list;
    }

    public void setList(ArrayList<FormOfPageModel> list) {
        this.list = list;
    }
}
