package com.mercado.paymentapp.model;

public class IssuerModel {
   private String id;
   private String name;
   private String secure_thumbnail;
   private String thumbnail;

   public IssuerModel(){}
   public IssuerModel(String pid,String pname,String psecure_thumbnail,String pthumbnail){
       this.id = pid;
       this.name = pname;
       this.secure_thumbnail = psecure_thumbnail;
       this.thumbnail = pthumbnail;
   }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSecure_thumbnail() {
        return secure_thumbnail;
    }

    public void setSecure_thumbnail(String secure_thumbnail) {
        this.secure_thumbnail = secure_thumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
