package com.mercado.paymentapp.model;

import java.util.ArrayList;

public class PayerCostModel {
  private int installments;
  private Double installment_rate;
  private int discount_rate;
  private ArrayList<String> labels;
  private ArrayList<String> installment_rate_collector;
  private int min_allowed_amount;
  private int max_allowed_amount;
  private String recommended_message;
  private Double installment_amount;
  private Double total_amount;

   public PayerCostModel(){}

    public PayerCostModel(Integer pinstallments,Double pinstallment_rate,int pdiscount_rate,ArrayList<String> plabels, ArrayList<String> pinstallment_rate_collector,int pmin_allowed_amount,int pmax_allowed_amount,String precommended_message,Double pinstallment_amount,Double ptotal_amount){
       this.installments = pinstallments;
       this.installment_rate = pinstallment_rate;
       this.discount_rate = pdiscount_rate;
       this.labels = plabels;
       this.installment_rate_collector = pinstallment_rate_collector;
       this.min_allowed_amount = pmin_allowed_amount;
       this.max_allowed_amount = pmax_allowed_amount;
       this.recommended_message = precommended_message;
       this.installment_amount = pinstallment_amount;
       this.total_amount = ptotal_amount;
    }

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public Double getInstallment_rate() {
        return installment_rate;
    }

    public void setInstallment_rate(Double installment_rate) {
        this.installment_rate = installment_rate;
    }

    public int getDiscount_rate() {
        return discount_rate;
    }

    public void setDiscount_rate(int discount_rate) {
        this.discount_rate = discount_rate;
    }

    public ArrayList<String> getLabels() {
        return labels;
    }

    public void setLabels(ArrayList<String> labels) {
        this.labels = labels;
    }

    public ArrayList<String> getInstallment_rate_collector() {
        return installment_rate_collector;
    }

    public void setInstallment_rate_collector(ArrayList<String> installment_rate_collector) {
        this.installment_rate_collector = installment_rate_collector;
    }

    public int getMin_allowed_amount() {
        return min_allowed_amount;
    }

    public void setMin_allowed_amount(int min_allowed_amount) {
        this.min_allowed_amount = min_allowed_amount;
    }

    public int getMax_allowed_amount() {
        return max_allowed_amount;
    }

    public void setMax_allowed_amount(int max_allowed_amount) {
        this.max_allowed_amount = max_allowed_amount;
    }

    public String getRecommended_message() {
        return recommended_message;
    }

    public void setRecommended_message(String recommended_message) {
        this.recommended_message = recommended_message;
    }

    public Double getInstallment_amount() {
        return installment_amount;
    }

    public void setInstallment_amount(Double installment_amount) {
        this.installment_amount = installment_amount;
    }

    public Double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
    }
}
