package com.mercado.paymentapp.model;

import java.util.ArrayList;

import javax.annotation.Nullable;

public class RecomendedMessageModel {
   private String payment_method_id;
   private String payment_type_id;
   private IssuerModel issuer;
   private String processing_mode;
   @Nullable
   private
   String merchant_account_id;
   private ArrayList<PayerCostModel> payer_costs;

    public RecomendedMessageModel(){}

    public RecomendedMessageModel(String ppayment_method_id,String ppayment_type_id,IssuerModel pissuer,String pprocessing_mode,String pmerchant_account_id,ArrayList<PayerCostModel> ppayer_costs){
        this.payment_method_id = ppayment_method_id;
        this.payment_type_id = ppayment_type_id;
        this.issuer = pissuer;
        this.processing_mode = pprocessing_mode;
        this.merchant_account_id = pmerchant_account_id;
        this.payer_costs = ppayer_costs;
    }

    public String getPayment_method_id() {
        return payment_method_id;
    }

    public void setPayment_method_id(String payment_method_id) {
        this.payment_method_id = payment_method_id;
    }

    public String getPayment_type_id() {
        return payment_type_id;
    }

    public void setPayment_type_id(String payment_type_id) {
        this.payment_type_id = payment_type_id;
    }

    public IssuerModel getIssuer() {
        return issuer;
    }

    public void setIssuer(IssuerModel issuer) {
        this.issuer = issuer;
    }

    public String getProcessing_mode() {
        return processing_mode;
    }

    public void setProcessing_mode(String processing_mode) {
        this.processing_mode = processing_mode;
    }

    @Nullable
    public String getMerchant_account_id() {
        return merchant_account_id;
    }

    public void setMerchant_account_id(@Nullable String merchant_account_id) {
        this.merchant_account_id = merchant_account_id;
    }

    public ArrayList<PayerCostModel> getPayer_costs() {
        return payer_costs;
    }

    public void setPayer_costs(ArrayList<PayerCostModel> payer_costs) {
        this.payer_costs = payer_costs;
    }
}
