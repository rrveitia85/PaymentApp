package com.mercado.paymentapp.model;

public class SecurityCode{
    private int length;
    private String card_location;
    private String mode;

    public SecurityCode(){}
    public SecurityCode(Integer plength,String pcard_location,String pmode){
      this.length = plength;
      this.card_location = pcard_location;
      this.mode = pmode;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getCard_location() {
        return card_location;
    }

    public void setCard_location(String card_location) {
        this.card_location = card_location;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }
}
