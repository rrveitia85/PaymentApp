package com.mercado.paymentapp.model;

import javax.annotation.Nullable;

public @Nullable
class SettingsDataModel {
    private CardNumber card_number;
    private BinModel bin;
    private SecurityCode security_code;

    public SettingsDataModel(){}

    public SettingsDataModel(CardNumber card_number,BinModel bin,SecurityCode security_code){
        this.card_number = card_number;
        this.bin = bin;
        this.security_code = security_code;
    }


    public CardNumber getCard_number() {
        return card_number;
    }

    public void setCard_number(CardNumber card_number) {
        this.card_number = card_number;
    }

    public BinModel getBin() {
        return bin;
    }

    public void setBin(BinModel bin) {
        this.bin = bin;
    }

    public SecurityCode getSecurity_code() {
        return security_code;
    }

    public void setSecurity_code(SecurityCode security_code) {
        this.security_code = security_code;
    }
}
