package com.mercado.paymentapp.network.api;

import com.mercado.paymentapp.model.FormOfPageModel;
import com.mercado.paymentapp.model.PaymenthMethodModel;
import com.mercado.paymentapp.model.RecomendedMessageModel;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ServicesApi {
//    @Headers("Content-Type:application/json")
    @GET("payment_methods")
    Call<ArrayList<FormOfPageModel>> getMedioPago(@Query("public_key") String apikey);

    @GET("payment_methods/card_issuers")
    Call<ArrayList<PaymenthMethodModel>> getPaymentMethods(@Query("public_key") String apikey, @Query("payment_method_id") String mediopago);

    @GET("payment_methods/installments")
    Call<ArrayList<RecomendedMessageModel>> getRecommendedMessage(@Query("public_key") String apikey, @Query("amount") String amount, @Query("payment_method_id") String mediopago, @Query("issuer.id") String issuerid);
}
