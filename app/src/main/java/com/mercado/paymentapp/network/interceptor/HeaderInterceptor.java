package com.mercado.paymentapp.network.interceptor;

import android.os.Build;

import com.mercado.paymentapp.BuildConfig;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import static com.mercado.paymentapp.utility.ConstantsUtils.ACCEPT;
import static com.mercado.paymentapp.utility.ConstantsUtils.APP_VERSION_CODE;
import static com.mercado.paymentapp.utility.ConstantsUtils.APP_VERSION_NAME;
import static com.mercado.paymentapp.utility.ConstantsUtils.USER_AGENT;

public class HeaderInterceptor implements Interceptor {
    String apiVersion = "application/json;charset=UTF-8";
    String appVersionName = BuildConfig.VERSION_NAME;
    String appVersionCode = String.valueOf(BuildConfig.VERSION_CODE);

    public String generateUserAgent(){
        return okhttp3.internal.Version.userAgent() + " (Linux; U; Android " + Build.VERSION.RELEASE + "; " + Build.MODEL + " Build/" + Build.DISPLAY + ") Mobile";
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request originalRequest =chain.request();
        Request newRequest = originalRequest.newBuilder()
//                .addHeader(APP_VERSION_NAME, appVersionName)
//                .addHeader(APP_VERSION_CODE, appVersionCode)
                .addHeader(USER_AGENT, generateUserAgent())
//                .addHeader("Accept", "gzip")
//                .addHeader("cache-control", "no-cache")
//                .addHeader("Content-Type","application/json;charset=UTF-8")
//                .addHeader(ACCEPT, apiVersion)
                .build();
        return chain.proceed(newRequest);
    }
}
