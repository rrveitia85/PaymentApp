package com.mercado.paymentapp.network.utils;

import com.mercado.paymentapp.network.api.ServicesApi;

import retrofit2.Retrofit;

public class ApiFactory {
    private Retrofit retrofit;

    public ApiFactory(Retrofit retrofitParam) {
        retrofit = retrofitParam;
    }

    public ServicesApi servicesApi() {
        return retrofit.create(ServicesApi.class);
    }
}
