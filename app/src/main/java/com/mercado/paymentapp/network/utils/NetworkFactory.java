package com.mercado.paymentapp.network.utils;

import com.mercado.paymentapp.BuildConfig;
import com.mercado.paymentapp.network.interceptor.HeaderInterceptor;

import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkFactory {
    private static String apiBaseUrl = BuildConfig.API_BASE_URL;

    public static Retrofit.Builder getBaseClient(){
        return new Retrofit.Builder()
                .baseUrl(apiBaseUrl)
                .addConverterFactory(GsonConverterFactory.create());
    }

    public static Retrofit getClient(){
        return getBaseClient()
                .client(getOkHttpClient())
                .build();
    }

    public static Retrofit getNoAuthClient(){
        return getBaseClient()
                .client(getNoAuthOkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static OkHttpClient getOkHttpClient(){
        return trustAllCerts()
                .addInterceptor(new HeaderInterceptor())
//                .addInterceptor(AuthenticatorHeaderInterceptor(tokenManager))
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .followRedirects(false)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
    }


    public static OkHttpClient getNoAuthOkHttpClient(){
        return trustAllCerts()
                .addInterceptor(new HeaderInterceptor())
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .followRedirects(false)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build();
    }

    public static OkHttpClient.Builder trustAllCerts(){
        try{
            TrustManager[] trustAllCerts = new TrustManager[1];
                    TrustManager simpleCert = new X509TrustManager(){

                @Override
                public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                }

                @Override
                public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {

                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };
            trustAllCerts[0] = simpleCert;

            final KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null, null);
            SSLContext sslContext = SSLContext.getInstance("TLS");
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, "keystore_pass".toCharArray());
            sslContext.init(null, trustAllCerts, new SecureRandom());

            OkHttpClient.Builder okHttp = new OkHttpClient.Builder();
            okHttp.sslSocketFactory(sslContext.getSocketFactory());
            okHttp.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String s, SSLSession sslSession) {
                    return true;
                }
            });
            return okHttp;
        }catch (Exception ex){

        }
        return null;
    }
}
