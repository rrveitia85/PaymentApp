package com.mercado.paymentapp.repository;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mercado.paymentapp.R;
import com.mercado.paymentapp.model.FormOfPageDataModel;
import com.mercado.paymentapp.model.FormOfPageModel;
import com.mercado.paymentapp.model.PaymenthMethodModel;
import com.mercado.paymentapp.model.RecomendedMessageModel;
import com.mercado.paymentapp.network.api.ServicesApi;
import com.mercado.paymentapp.network.utils.ApiFactory;
import com.mercado.paymentapp.network.utils.NetworkFactory;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataRepository implements DataRepositoryInterface {

    ServicesApi serviceApiCall;
    Context context;

    public DataRepository(Context contextParam){
        serviceApiCall = new ApiFactory(NetworkFactory.getNoAuthClient()).servicesApi();
        context = contextParam;
    }

    @Override
    public Call<ArrayList<FormOfPageModel>> getMediosDePago(String apikey) {
        return serviceApiCall.getMedioPago(apikey);
    }

    @Override
    public Call<ArrayList<PaymenthMethodModel>> getPaymentMethods(String apikey, String mediopago) {
        return serviceApiCall.getPaymentMethods(apikey,mediopago);
    }

    @Override
    public Call<ArrayList<RecomendedMessageModel>> getRecommendedMessage(String apikey, String amount, String mediopago, String issuerid) {
        return serviceApiCall.getRecommendedMessage(apikey,amount,mediopago,issuerid);
    }
}
