package com.mercado.paymentapp.repository;

import com.mercado.paymentapp.model.FormOfPageModel;
import com.mercado.paymentapp.model.PaymenthMethodModel;
import com.mercado.paymentapp.model.RecomendedMessageModel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Path;

public interface DataRepositoryInterface {
    Call<ArrayList<FormOfPageModel>> getMediosDePago(String apikey);
    Call<ArrayList<PaymenthMethodModel>> getPaymentMethods(String apikey,String mediopago);
    Call<ArrayList<RecomendedMessageModel>> getRecommendedMessage(String apikey, String amount, String mediopago, String issuerid);
}
