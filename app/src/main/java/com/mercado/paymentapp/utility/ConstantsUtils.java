package com.mercado.paymentapp.utility;

public class ConstantsUtils {
    public static String CLOSE_APP_DIALOG = "CloseAppDialog";

    public static String SEND_DATA_FRAGMENT = "TransactionModelData";
    public static String APP_VERSION_NAME = "X-App-VersionName";
    public static String APP_VERSION_CODE = "X-App-VersionCode";
    public static String USER_AGENT = "User-Agent";
    public static String ACCEPT = "Accept";
    public static String ERRROR_DIALOG = "ErrorDialog";

    public static String SHAREDPREFERENCES_SETTINGS_NAME = "com.paymentapp.settingsData";
    public static String TOKEN_USER = "com.paymentapp.tokenUser";
    public static String REFRESH_TOKEN_USER = "com.paymentapp.refreshTokenUser";
    public static String IS_USER_LOGGED = "com.paymentapp.isUserLogged";
    public static String ICON_SELECTED = "com.paymentapp.indexIconSelected";
    public static String IDIOM_SELECTED = "com.paymentapp.indexIdiomSelected";
    public static String NEVER_ASK_AGAIN ="com.paymentapp.neverAskAgain";

}
