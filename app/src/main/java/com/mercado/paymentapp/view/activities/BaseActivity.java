package com.mercado.paymentapp.view.activities;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.mercado.paymentapp.R;
import com.mercado.paymentapp.dialog.CloseAppDialog;
import com.mercado.paymentapp.dialog.ListenerClickInterface;
import com.mercado.paymentapp.utility.ConstantsUtils;

import java.util.ArrayList;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class BaseActivity extends AppCompatActivity {
    private Integer navHostFragmentId;
    protected Integer[] exitingDestinations;
    protected ArrayList<NavigateBack> navigateBackListeners = new ArrayList();
    public CloseAppDialog closeAppFunction;

    public BaseActivity(Integer navHostFragmentId, Integer[] exitingDestinations){
        this.navHostFragmentId = navHostFragmentId;
        this.exitingDestinations = exitingDestinations;
    }

    public void addNavigateBackListener(NavigateBack fragmentNavigate) {
        navigateBackListeners.add(fragmentNavigate);
    }

    public void removeNavigateBackListener(NavigateBack fragmentNavigate) {
        navigateBackListeners.remove(fragmentNavigate);
    }

    public NavController getNavController(){
        return Navigation.findNavController(this,navHostFragmentId);
    }

    @Override
    public void onBackPressed() {
        Boolean canNav = true;
        for (NavigateBack items:navigateBackListeners){
            if(items.canNavigateBack()){
               canNav = false;
            }
        }
        if(canNav){
            normalNavigation();
        }
    }

    public void createCloseDialog(){
        if(closeAppFunction==null){
            closeAppFunction = new CloseAppDialog();
            closeAppFunction.setListeners(new ListenerClickInterface() {
                @Override
                public void onClickPositive(DialogFragment dialog) {
                    finish();
                    dialog.dismiss();
                }

                @Override
                public void onClickCancelar(DialogFragment dialog) {
                    dialog.dismiss();
                }
            });
        }
        closeAppFunction.show(this.getSupportFragmentManager(), ConstantsUtils.CLOSE_APP_DIALOG);
    }

    public Boolean normalNavigation(){
        if (findInList()
//                && Navigation.findNavController(this, R.id.navHostFragment).getCurrentDestination().getId() == R.id.homeFragment
        ){
            createCloseDialog();
            return true;
        }else {
           return Navigation.findNavController(this, R.id.navHostFragment).popBackStack();
        }
    }

    public Boolean findInList(){
       Boolean isInList = false;
        for(Integer items: exitingDestinations) {
           if(items == Navigation.findNavController(this, R.id.navHostFragment).getCurrentDestination().getId()){
             isInList = true;
           }
       }
       return isInList;
    }

}
