package com.mercado.paymentapp.view.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.mercado.paymentapp.R;

import javax.annotation.Nullable;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;

public class MainActivity extends BaseActivity implements Navigator {

    public MainActivity() {
        super(R.id.navHostFragment, new Integer[]{R.id.amountEnterFragment});
    }

    public MainActivity(Integer navHostFragmentId, Integer[] exitingDestinations) {
        super(navHostFragmentId, exitingDestinations);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    public NavController getNavigationController() {
        return Navigation.findNavController(this, R.id.navHostFragment);
    }

    @Override
    public void navigateTo(Integer destinationId, @Nullable Bundle bundleTo) {
        getNavigationController().navigate(destinationId, bundleTo);
    }

    @Override
    public void navigateBack(Integer destination, Boolean inclusive) {
        getNavigationController().popBackStack(destination, inclusive);
    }

    @Override
    public void navigeteBack() {
        getNavigationController().popBackStack();
    }
}
