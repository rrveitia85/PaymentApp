package com.mercado.paymentapp.view.activities;

public interface NavigateBack {
    public Boolean canNavigateBack();
}
