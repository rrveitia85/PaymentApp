package com.mercado.paymentapp.view.activities;

import android.os.Bundle;

import javax.annotation.Nullable;

import androidx.navigation.NavController;

public interface Navigator {
    NavController getNavigationController();
    void navigateTo(Integer destinationId,@Nullable Bundle bundleTo);
    void navigateBack(Integer destination,Boolean inclusive);
    void navigeteBack();
}
