package com.mercado.paymentapp.view.activities;

public interface ToolbarListenerActivity {
    public Boolean isWithToolbar();
    public int iconBack();
    public String getTitleToolbar();
}
