package com.mercado.paymentapp.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mercado.paymentapp.R;
import com.mercado.paymentapp.dialog.DialogAlert;
import com.mercado.paymentapp.dialog.ListenerClickInterface;
import com.mercado.paymentapp.model.TransactionModel;
import com.mercado.paymentapp.utility.ConstantsUtils;
import com.mercado.paymentapp.view.activities.MainActivity;
import com.mercado.paymentapp.view.activities.Navigator;
import com.mercado.paymentapp.view.viewmodel.DataViewModel;

import androidx.navigation.NavController;

import static com.mercado.paymentapp.utility.ConstantsUtils.ERRROR_DIALOG;

public class AmountEnterFragment extends BaseFragment implements View.OnClickListener {

    private EditText inputAmount;
    private Button acceptBtn;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewRoot = inflater.inflate(R.layout.amount_enter_layout, container, false);
//        setHasOptionsMenu(true);
        return viewRoot;
    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    public void setData(){
        inputAmount = viewRoot.findViewById(R.id.inputAmountEt);
        acceptBtn = viewRoot.findViewById(R.id.accpetBtn);
        acceptBtn.setOnClickListener(this);

        if(DataViewModel.getTransactionModel()!=null){
            if(DataViewModel.getTransactionModel().getAmount()!=null && DataViewModel.getTransactionModel().getMedioPago()!=null && DataViewModel.getTransactionModel().getUserId()!=null && DataViewModel.getTransactionModel().getRecommendedMessage()!=null){
                DialogAlert alertMessage = new DialogAlert();
                String meessageShow = String.format(_getString(R.string.message_final),DataViewModel.getTransactionModel().getAmount().toString(),DataViewModel.getTransactionModel().getMedioPago(),DataViewModel.getTransactionModel().getUserId(),DataViewModel.getTransactionModel().getRecommendedMessage());
                alertMessage.setMessage(meessageShow);
                alertMessage.listener = new ListenerClickInterface() {
                    @Override
                    public void onClickPositive(DialogFragment dialog) {
                        DataViewModel.setTransactionModel(null);
                        dialog.dismiss();
                    }

                    @Override
                    public void onClickCancelar(DialogFragment dialog) {

                    }
                };
                if(!alertMessage.isAdded()){
                    alertMessage.show(getChildFragmentManager(),ERRROR_DIALOG);
                }
            }
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId()== acceptBtn.getId()){
            if(!inputAmount.getText().toString().equals("")){
                DataViewModel.setTransactionModel(new TransactionModel());
                TransactionModel transaction = new TransactionModel();
                transaction.setAmount(Double.parseDouble(inputAmount.getText().toString()));
                Bundle bundleParam = new Bundle();
                bundleParam.putParcelable(ConstantsUtils.SEND_DATA_FRAGMENT,transaction);
                navigateTo(R.id.action_amountEnterFragment_to_paymentsMethodsFragment,bundleParam);
            }else{
                Toast.makeText(getContext(),_getString(R.string.please_enter_amount),Toast.LENGTH_SHORT).show();
            }
        }
    }
}
