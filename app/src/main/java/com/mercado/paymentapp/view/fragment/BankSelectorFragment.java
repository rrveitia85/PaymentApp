package com.mercado.paymentapp.view.fragment;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mercado.paymentapp.R;
import com.mercado.paymentapp.adapter.BankAdapter;
import com.mercado.paymentapp.adapter.OnClickSelectedListener;
import com.mercado.paymentapp.adapter.PaymentMethodsAdapter;
import com.mercado.paymentapp.dialog.DialogAlert;
import com.mercado.paymentapp.dialog.ListenerClickInterface;
import com.mercado.paymentapp.model.PaymenthMethodModel;
import com.mercado.paymentapp.model.TransactionModel;
import com.mercado.paymentapp.utility.ConstantsUtils;
import com.mercado.paymentapp.view.viewmodel.DataViewModel;

import java.util.ArrayList;

import static com.mercado.paymentapp.utility.ConstantsUtils.ERRROR_DIALOG;

public class BankSelectorFragment extends BaseFragment implements OnClickSelectedListener {

    private TransactionModel transaction;
    private BankAdapter adapterBank;
    private RecyclerView listBanksRv;
    private ArrayList<PaymenthMethodModel> listBanks = new ArrayList<>();
    private DataViewModel dataViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewRoot = inflater.inflate(R.layout.bank_select_layout, container, false);
//        setHasOptionsMenu(true);
        return viewRoot;
    }

    @Override
    public void parselFragmentData() {
        Object dataBundle = getArguments().get(ConstantsUtils.SEND_DATA_FRAGMENT);
        if (dataBundle!=null){
            if(dataBundle instanceof TransactionModel){
                transaction = (TransactionModel) dataBundle;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUiData();
    }

    public void setUiData(){
        if (dataViewModel == null) {
            dataViewModel = new DataViewModel(getContext());
        }
        listBanksRv = viewRoot.findViewById(R.id.listBanksRv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listBanksRv.setLayoutManager(llm);
        listBanksRv.setHasFixedSize(true);
        adapterBank = new BankAdapter(getContext(),listBanks, this);
        listBanksRv.setAdapter(adapterBank);
        dataViewModel.getListBanks().observe(this, new Observer<ArrayList<PaymenthMethodModel>>() {
            @Override
            public void onChanged(@Nullable ArrayList<PaymenthMethodModel> paymenthMethodModels) {
                if(paymenthMethodModels!=null){
                    if(paymenthMethodModels.size() == 0){
                        DialogAlert alertMessage = new DialogAlert();
                        alertMessage.setMessage(_getString(R.string.meesage_no_banks));
                        alertMessage.listener = new ListenerClickInterface() {
                            @Override
                            public void onClickPositive(DialogFragment dialog) {
                                navigateBack(R.id.paymentsMethodsFragment,false);
                                dialog.dismiss();
                            }

                            @Override
                            public void onClickCancelar(DialogFragment dialog) {

                            }
                        };
                        if(!alertMessage.isAdded()){
                           alertMessage.show(getChildFragmentManager(),ERRROR_DIALOG);
                        }
                    }else{
                        listBanks.clear();
                        listBanks.addAll(paymenthMethodModels);
                        adapterBank.setTransactionArrayList(listBanks);
                        adapterBank.notifyDataSetChanged();
                    }
                }
            }
        });
        if(transaction!=null){
            dataViewModel.getListOfBanks(transaction.getMedioPago());
        }
    }

    @Override
    public void onItemSelected(int position) {
        if(listBanks!=null){
            PaymenthMethodModel tempModel = listBanks.get(position);
            transaction.setUserId(tempModel.getId());
            Bundle bundleToData = new Bundle();
            bundleToData.putParcelable(ConstantsUtils.SEND_DATA_FRAGMENT,transaction);
            navigateTo(R.id.action_bankSelectorFragment_to_recomendedFragment,bundleToData);
        }
    }
}
