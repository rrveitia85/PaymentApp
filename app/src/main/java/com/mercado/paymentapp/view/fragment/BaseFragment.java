package com.mercado.paymentapp.view.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

import com.mercado.paymentapp.R;
import com.mercado.paymentapp.view.activities.NavigateBack;
import com.mercado.paymentapp.view.activities.ToolbarListenerActivity;

import androidx.navigation.Navigation;

public class BaseFragment extends Fragment implements NavigateBack {
    protected View viewRoot;

    @Override
    public Boolean canNavigateBack() {
        return true;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        parselFragmentData();
    }

    public void parselFragmentData() {

    }

    public Resources _getResources() {
        return getContext().getResources();
    }

    public String _getString(Integer resId) {
        return getContext().getResources().getString(resId);
    }

    public void navigateTo(Integer destinationId, @Nullable Bundle bundleTo) {
        if (destinationId != -1) {
            if (bundleTo != null) {
                Navigation.findNavController(getActivity(), R.id.navHostFragment).navigate(destinationId, bundleTo);
            } else {
                Navigation.findNavController(getActivity(), R.id.navHostFragment).navigate(destinationId);
            }
        }
    }

    public void navigateBack(Integer destination, Boolean inclusive) {
        if (destination != -1) {
            Navigation.findNavController(getActivity(), R.id.navHostFragment).popBackStack(destination, inclusive);
        } else {
            Navigation.findNavController(getActivity(), R.id.navHostFragment).popBackStack();
        }
    }
}
