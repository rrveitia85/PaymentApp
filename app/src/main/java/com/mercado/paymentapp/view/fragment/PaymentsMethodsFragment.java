package com.mercado.paymentapp.view.fragment;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mercado.paymentapp.R;
import com.mercado.paymentapp.adapter.OnClickSelectedListener;
import com.mercado.paymentapp.adapter.PaymentMethodsAdapter;
import com.mercado.paymentapp.model.FormOfPageModel;
import com.mercado.paymentapp.model.TransactionModel;
import com.mercado.paymentapp.utility.ConstantsUtils;
import com.mercado.paymentapp.view.viewmodel.DataViewModel;

import java.util.ArrayList;

public class PaymentsMethodsFragment extends BaseFragment implements OnClickSelectedListener {
    private TextView titlePaymentMethods;
    private RecyclerView listPaymentMethods;
    private PaymentMethodsAdapter adapterPayment;
    public ArrayList<FormOfPageModel> transactionArrayListParam = new ArrayList();
    public DataViewModel dataViewModel;
    private TransactionModel transaction;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewRoot = inflater.inflate(R.layout.payment_method_layout, container, false);
//        setHasOptionsMenu(true);
        return viewRoot;
    }

    @Override
    public void parselFragmentData() {
       Object dataBundle = getArguments().get(ConstantsUtils.SEND_DATA_FRAGMENT);
       if (dataBundle!=null){
           if(dataBundle instanceof TransactionModel){
             transaction = (TransactionModel) dataBundle;
           }
       }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUiData();
    }

    private void setUiData() {
        if (dataViewModel == null) {
            dataViewModel = new DataViewModel(getContext());
        }
        listPaymentMethods = viewRoot.findViewById(R.id.listPaymentMethodsRv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listPaymentMethods.setLayoutManager(llm);
        listPaymentMethods.setHasFixedSize(true);
        adapterPayment = new PaymentMethodsAdapter(transactionArrayListParam, getContext(), this);
        listPaymentMethods.setAdapter(adapterPayment);
        dataViewModel.getListFormOfPage().observe(this, new Observer<ArrayList<FormOfPageModel>>() {
            @Override
            public void onChanged(@Nullable ArrayList<FormOfPageModel> formOfPageModels) {
                transactionArrayListParam.clear();
                transactionArrayListParam.addAll(formOfPageModels);
                adapterPayment.setTransactionArrayList(transactionArrayListParam);
                adapterPayment.notifyDataSetChanged();
            }
        });
        if(transactionArrayListParam.size()==0){
            dataViewModel.getListOfFormPage();
        }
    }

    @Override
    public void onItemSelected(int position) {
        FormOfPageModel tempObject = transactionArrayListParam.get(position);
        if (tempObject!=null && transaction!=null){
           transaction.setMedioPago(tempObject.getId());
           Bundle bundleTo = new Bundle();
           bundleTo.putParcelable(ConstantsUtils.SEND_DATA_FRAGMENT,transaction);
           navigateTo(R.id.action_paymentsMethodsFragment_to_bankSelectorFragment,bundleTo);
        }
    }

    public TransactionModel getTransaction() {
        return transaction;
    }

    public void setTransaction(TransactionModel transaction) {
        this.transaction = transaction;
    }
}
