package com.mercado.paymentapp.view.fragment;

import android.arch.lifecycle.Observer;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mercado.paymentapp.R;
import com.mercado.paymentapp.adapter.BankAdapter;
import com.mercado.paymentapp.adapter.OnClickSelectedListener;
import com.mercado.paymentapp.adapter.RecommendedAdapter;
import com.mercado.paymentapp.model.PayerCostModel;
import com.mercado.paymentapp.model.PaymenthMethodModel;
import com.mercado.paymentapp.model.RecomendedMessageModel;
import com.mercado.paymentapp.model.TransactionModel;
import com.mercado.paymentapp.utility.ConstantsUtils;
import com.mercado.paymentapp.view.viewmodel.DataViewModel;

import java.util.ArrayList;

public class RecomendedFragment extends BaseFragment implements OnClickSelectedListener {
    private RecommendedAdapter adapterRecomended;
    private RecyclerView listRecomendedRv;
    private ArrayList<PayerCostModel> listRecomendation = new ArrayList();
    private TransactionModel transactionModel;
    private DataViewModel dataViewModel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewRoot = inflater.inflate(R.layout.recommended_layout, container, false);
//        setHasOptionsMenu(true);
        return viewRoot;
    }

    @Override
    public void parselFragmentData() {
        Object dataBundle = getArguments().get(ConstantsUtils.SEND_DATA_FRAGMENT);
        if (dataBundle!=null){
            if(dataBundle instanceof TransactionModel){
                transactionModel = (TransactionModel) dataBundle;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUiData();
    }

    public void setUiData(){
        if (dataViewModel == null) {
            dataViewModel = new DataViewModel(getContext());
        }
        listRecomendedRv = viewRoot.findViewById(R.id.listRecomendedRv);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listRecomendedRv.setLayoutManager(llm);
        listRecomendedRv.setHasFixedSize(true);
        adapterRecomended = new RecommendedAdapter(getContext(),listRecomendation, this);
        listRecomendedRv.setAdapter(adapterRecomended);

        dataViewModel.getListMessagesRecommended().observe(this, new Observer<RecomendedMessageModel>() {
            @Override
            public void onChanged(@Nullable RecomendedMessageModel recomendedMessageModel) {
                listRecomendation.clear();
                listRecomendation.addAll(recomendedMessageModel.getPayer_costs());
                adapterRecomended.setRecomendedList(listRecomendation);
                adapterRecomended.notifyDataSetChanged();
            }
        });

        if(transactionModel!=null){
            dataViewModel.getListOfRecommendedMessage(transactionModel.getAmount().toString(),transactionModel.getMedioPago(),transactionModel.getUserId());
        }
    }

    @Override
    public void onItemSelected(int position) {
        if(transactionModel!=null){
            transactionModel.setRecommendedMessage(listRecomendation.get(position).getRecommended_message());
            DataViewModel.setTransactionModel(transactionModel);
            navigateBack(R.id.amountEnterFragment,false);
        }
    }
}
