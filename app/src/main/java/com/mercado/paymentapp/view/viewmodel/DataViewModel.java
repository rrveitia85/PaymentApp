package com.mercado.paymentapp.view.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.widget.Toast;

import com.mercado.paymentapp.BuildConfig;
import com.mercado.paymentapp.R;
import com.mercado.paymentapp.model.FormOfPageModel;
import com.mercado.paymentapp.model.PaymenthMethodModel;
import com.mercado.paymentapp.model.RecomendedMessageModel;
import com.mercado.paymentapp.model.TransactionModel;
import com.mercado.paymentapp.repository.DataRepository;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataViewModel extends ViewModel {
    DataRepository dataRepository;
    private MutableLiveData<ArrayList<FormOfPageModel>> listFormOfPage = new MutableLiveData<>();
    private MutableLiveData<ArrayList<PaymenthMethodModel>> listBanks = new MutableLiveData<>();
    private MutableLiveData<RecomendedMessageModel> listMessagesRecommended = new MutableLiveData<>();
    private Context contex;

    private static TransactionModel transactionModel;
    public DataViewModel(Context context){
        super();
        this.contex = context;
        if(dataRepository == null){
            dataRepository = new DataRepository(context);
        }
    }

    public static TransactionModel getTransactionModel() {
        return transactionModel;
    }

    public static void setTransactionModel(TransactionModel transactionModel) {
        DataViewModel.transactionModel = transactionModel;
    }

    public void getListOfFormPage(){
     Call<ArrayList<FormOfPageModel>> listCall =  dataRepository.getMediosDePago(BuildConfig.API_KEY);
        listCall.enqueue(new Callback<ArrayList<FormOfPageModel>>() {
            @Override
            public void onResponse(Call<ArrayList<FormOfPageModel>> call, Response<ArrayList<FormOfPageModel>> response) {
                if(response.isSuccessful()){
                    if(response.body()!=null){
                        ArrayList<FormOfPageModel> list = response.body();
//                        Gson gson = new Gson();
//                        ArrayList<String> list = gson.fromJson(response.body().toString(),  new TypeToken<List<String>>(){}.getType());
//                        ArrayList<String> temps =list;
                        getListFormOfPage().postValue(response.body());
                    }else{
                        Toast.makeText(contex, contex.getResources().getString(R.string.error_no_data), Toast.LENGTH_SHORT).show();
                    }
                }else{

                }
            }
            @Override
            public void onFailure(Call<ArrayList<FormOfPageModel>> call, Throwable t) {
                Toast.makeText(contex, contex.getResources().getString(R.string.error_obtain_data), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getListOfBanks(String medioPago){
        Call<ArrayList<PaymenthMethodModel>> listBanksCall = dataRepository.getPaymentMethods(BuildConfig.API_KEY,medioPago);
        listBanksCall.enqueue(new Callback<ArrayList<PaymenthMethodModel>>() {
            @Override
            public void onResponse(Call<ArrayList<PaymenthMethodModel>> call, Response<ArrayList<PaymenthMethodModel>> response) {
                if(response.isSuccessful()){
                    if(response.body()!=null){
                        ArrayList<PaymenthMethodModel> listBanksTemp = response.body();
                        listBanks.postValue(listBanksTemp);
                    }else{
                        Toast.makeText(contex, contex.getResources().getString(R.string.error_no_data), Toast.LENGTH_SHORT).show();
                    }
                }else{

                }
            }

            @Override
            public void onFailure(Call<ArrayList<PaymenthMethodModel>> call, Throwable t) {
                Toast.makeText(contex, contex.getResources().getString(R.string.error_obtain_data), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getListOfRecommendedMessage(String amount,String paymentMethod,String issuerId){
       Call<ArrayList<RecomendedMessageModel>> listMessages = dataRepository.getRecommendedMessage(BuildConfig.API_KEY,amount,paymentMethod,issuerId);
       listMessages.enqueue(new Callback<ArrayList<RecomendedMessageModel>>() {
           @Override
           public void onResponse(Call<ArrayList<RecomendedMessageModel>> call, Response<ArrayList<RecomendedMessageModel>> response) {
               if(response.isSuccessful()){
                   if(response.body()!=null){
                       ArrayList<RecomendedMessageModel> recommendedList = response.body();
                       if(recommendedList.size()>0){
                           getListMessagesRecommended().postValue(recommendedList.get(0));
                       }
                   }else{
                       Toast.makeText(contex, contex.getResources().getString(R.string.error_no_data), Toast.LENGTH_SHORT).show();
                   }
               }else{

               }
           }

           @Override
           public void onFailure(Call<ArrayList<RecomendedMessageModel>> call, Throwable t) {
               Toast.makeText(contex, contex.getResources().getString(R.string.error_obtain_data), Toast.LENGTH_SHORT).show();
           }
       });
    }

    public Context getContex() {
        return contex;
    }

    public void setContex(Context contex) {
        this.contex = contex;
    }

    public MutableLiveData<ArrayList<FormOfPageModel>> getListFormOfPage() {
        return listFormOfPage;
    }

    public void setListFormOfPage(MutableLiveData<ArrayList<FormOfPageModel>> listFormOfPage) {
        this.listFormOfPage = listFormOfPage;
    }

    public MutableLiveData<ArrayList<PaymenthMethodModel>> getListBanks() {
        return listBanks;
    }

    public void setListBanks(MutableLiveData<ArrayList<PaymenthMethodModel>> listBanks) {
        this.listBanks = listBanks;
    }

    public MutableLiveData<RecomendedMessageModel> getListMessagesRecommended() {
        return listMessagesRecommended;
    }

    public void setListMessagesRecommended(MutableLiveData<RecomendedMessageModel> listMessagesRecommended) {
        this.listMessagesRecommended = listMessagesRecommended;
    }
}
